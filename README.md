# MELTME - CCA Backend Configuration #

There are configuration of CCA Backend Proxy/Gateway.
Please melt(apply) config files to server.

### What is this repository for? ###
- default congifuration of Keycloak, Kong, Nginx of CCA Backend

### Project Structure ###

```
meltme
    └── keycloak
    └── kong
    └── nginx
```

### Dependencies ###
- keycloak
- kong
- nginx
- docker 17.06

### Who do I talk to? ###
- Repo owner or admin